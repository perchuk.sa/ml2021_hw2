# ML2021_HW2

ml mipt 2021, homework 2. https://www.kaggle.com/c/home-credit-default-risk

### Prepare environment

1. Set up virtual environment
```
python3 -m venv env 
source env/bin/activate
```
To deactivate:
```
deactivate
```

2. Install requirements
```
pip install --upgrade pip
pip install -r requirements.txt
```

3. Download data
```
bash data/download_data.sh
```

4. Notebook is located in /notebooks folder.

*Note: kaggle submissions are commented out!*